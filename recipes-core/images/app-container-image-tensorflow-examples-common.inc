# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

# for the container to have some shell:
IMAGE_INSTALL += " \
        busybox \
        tensorflow \
"

# we'll add here the tensorflow examples
IMAGE_INSTALL += "celeb-face-matcher \
                 "

#IMAGE_INSTALL += "tf2-qs-for-beg \
#                  tf2-qs-for-exp \
#                  celeb-face-matcher \
#                 "

