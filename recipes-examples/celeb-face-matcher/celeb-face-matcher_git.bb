HOMEPAGE = "https://github.com/JanderHungrige/CelebFaceMatcher"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = "git://github.com/RobertBerger/tf2-examples;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/${BPN}"

# --> python checks
#inherit sca
#inherit sca-python-checks
# <-- python checks
inherit setuptools3

DEPENDS += "tensorflow \
            python3-numpy \
            opencv \
           "

RDEPENDS_${PN} += "python3-core \
                   python3-io \
                   python3-tensorflow \
                   util-linux-lscpu \
                   celeb-face-matcher-models \
                   python3-opencv \
                   opencv-apps"
