# Those are tensorflow models
HOMEPAGE = "https://github.com/JanderHungrige/CelebFaceMatcher"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

# we get something from amazon drive -> the real name is demo-celebrity-face-match-data-1.0.tar.gz
# due to the renaming the tarball is automatically unpacked (into demo-data)
SRC_URI = "https://content-na.drive.amazonaws.com/v2/download/presigned/fwO0dCZ3ovSi-RoZekQ9DGd89toPDVVVDN_loU3ZnfopX92IB?download=true;downloadfilename=demo-celebrity-face-match-data-1.0.tar.gz"
SRC_URI[sha256sum] = "b23001c4aea7621958654059e7d750799334bb3e0f994adb585166b56beb419c"

# we bundle whatever is under ${WORKDIR}/demo-data into the main package
do_install () {
	install -d ${D}${datadir}
        cp -R --no-dereference --preserve=mode,links -v ${WORKDIR}/demo-data ${D}${datadir}/${PN}/
}
