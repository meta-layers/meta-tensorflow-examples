HOMEPAGE = "https://apimirror.com/tensorflow~guide/quickstart/advanced"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = "git://github.com/RobertBerger/tf2-examples;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
#SRCREV = "b20272924469b4207eedd8dd83e234d719b6c16c"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/${BPN}"

inherit setuptools3

RDEPENDS_${PN} += "python3-core python3-io python3-tensorflow"
